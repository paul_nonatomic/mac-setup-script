# Install RVM
\curl -sSL https://get.rvm.io | bash -s stable

# Install Brew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Brew Taps
echo $'\n====== Install Via Brew Taps ======'
sudo brew tap $(cat brewTapFile|grep -v "#")

# Install Brew Modules
echo $'\n====== Install Via Brew ======'
sudo brew install $(cat brewFile|grep -v "#")
brew cleanup

# Install Cask Modules
echo $'\n====== Install Via Brew Cask ======'
sudo brew cask install $(cat caskFile|grep -v "#")
brew cask cleanup

# Install Node Modules
echo $'\n====== Install Via NPM ======'
sudo npm install $(cat nodeFile|grep -v "#")

# Set System Prefs
echo $'\n====== Setting System Prefs ======'

echo 'Auto quit printer app once print job completes'
defaults write com.apple.print.PrintingPrefs "Quit When Finished" -bool true

echo "Increase sound quality for Bluetooth headphones/headsets"
defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)" -int 40

echo "Setting trackpad & mouse speed"
defaults write -g com.apple.trackpad.scaling 2
defaults write -g com.apple.mouse.scaling 2.5

echo "Show hidden files in Finder by default"
defaults write com.apple.Finder AppleShowAllFiles -bool true

echo "Show dotfiles in Finder by default"
defaults write com.apple.finder AppleShowAllFiles TRUE

echo "Show all filename extensions in Finder by default"
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

echo "Disable the warning when changing a file extension"
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

echo "Use column view in all Finder windows by default"
defaults write com.apple.finder FXPreferredViewStyle Clmv

echo "Disable disk image verification"
defaults write com.apple.frameworks.diskimages skip-verify -bool true
defaults write com.apple.frameworks.diskimages skip-verify-locked -bool true

echo "Allowing text selection in Quick Look/Preview in Finder by default"
defaults write com.apple.finder QLEnableTextSelection -bool true

echo "Setting the icon size of Dock items to 36 pixels for optimal size/screen-realestate"
defaults write com.apple.dock tilesize -int 36
defaults write com.apple.frameworks.diskimages skip-verify-remote -bool true

echo "Setting email addresses to copy as 'foo@example.com' instead of 'Foo Bar <foo@example.com>' in Mail.app"
defaults write com.apple.mail AddressesIncludeNameOnPasteboard -bool false

echo "Enabling UTF-8 ONLY in Terminal.app and setting the Pro theme by default"
defaults write com.apple.terminal StringEncodings -array 4
defaults write com.apple.Terminal "Default Window Settings" -string "Pro"
defaults write com.apple.Terminal "Startup Window Settings" -string "Pro"

echo "Setting Git to use Sublime Text as default editor"
git config --global core.editor "subl -n -w"

